# Uvod
- Firewally chrani pred nepovloenym pristupem k aplikacim a sluzba v siti
- Existuji hw i sw reseni, ktera vytvareji pravidla pro pristup k aplikacim a sluzbam a na jejich zaklade filtruji pakety pri vstupu do site
- Na cisco routerech lze konfigurovat jednoduchy paketovy filtr pouzitim ACL

# Packetovy filtr
- kontorluje pristup do site pomoci analyzy prichozich a odchozich packetu
- propousti je ci zahazuje na zaklade splneni urcitych kriterii, napr
	- zdrojova ip adresa
	- cilova IP adresa
	- typ zapouzdreneho protokolu
	- port aplikace

# ACLs
- je posloupnost pravidel ve kterych je povolen ci zakazan pristup paketu do casti site na zaklade informaci z hlavicek protokolu vyssich vrstev
- pravidla = entries

# Druhy ACLs
## Standard ACLs
- filtruji se pouze na zaklade zdrojove IP adresy
## Extended ACLs - filtruji packety dle dalsich atributu (cilova IP, porty, druh protokolu)

## Cinnosti ACL
- na packety, ktere vstupuji pres interface a router = inbound traffic
- na pakety ktere prochazeji routerem
- na pakety ktere odchazeji pres interface z routeru = outbound traffic
- ACLse neaplikuji na pakety, ktere vzniknou na routeru

## Inbound ACL
- pakety prichazejici na router jsou kontrolovany predtim, nez jsou routovany
- inbound acl je efektivnejsi, protoze se zbytecne neroutiji packety, ktere budou zahozeny
- hodi se aplikovat na port do internetu

## Outbound ACL
- pravidla se aplikuji az je paket odeslan na vystupni interface
- je vhodne nasadit tam kde pakety z vice vstupnich interface odchazeji pres jeden vystupni interface

## Politika ACL

## Cisla a jmena acl

- acl muzeme oznacit cislem nebo pojemnovat
- pojmenovani umozni sndanejsi orientaci v ACL, nazev muze specifikovat pouziti
- Standard acls = 1 - 99, 1300 - 1999
- Extended acls = 100 - 199, 2000, 2699

### Named acl
- assign a name to identify the acl
- names can contain alpahunmeric characters
- it is suggested that the name be written in  CAPITAL LETTERS
- names cannot contain spaces or punctation
- entries can be added or deleted within the ACL

## Obecna pravidla pro tvorbu ACLs

- acls aplikujte na routeru, ktery je umisten mezi vasi interni siti a internetem (hranicni router)
- ACLs aplikuje na routeru, ktery spojuje nekolik casti vasi site, muzeme tak ridit provoz mezi castmi site
- Konfigurujte ACLs pro kazdy siitovy protokol, ktery router pouziva

## The three Ps
- jedno acls pro kazdy porotkol (ipv4 i 6)
- jedno acl pro kazdy smer (inbound a outbound)
- jedna sada acl pro kazde sitove rozhrani
- zalezi na poradi v retezci!

## Kam acl umistit

- acl je nutne vzdy umistit tam, kde bude jeho ucinek nejvetsi:
- extended acl co nejblize 

## Konfigurace standard acls

```
access-list <num> deny permit remark <source> [ source-wildcard ] [ log ]
no access-list <num>
```
- remark slouzi k okomentovani ACL (snazsi orientace)
```
ip access-group { num | name } { in | out }
no ip access-group { in | out }
```
```
S1(config)# access-list 1 permit 192.168.10.0 0.0.0.255
S1(config)# interface s0/0/0
S1(config-if)# ip access-group 1 out
```

## Vytvoreni named standard acl

```
ip access-list [ standard | extended ] name
Router(config-std-nac1)# [ permit | deny | remark ] { source [ source-wildcard ] } [ log ]
```

## Komentovane ACLs

- remark

- established = povoluje jiz navazana spojeni, (odpovedi)
